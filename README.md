# Biblioteca

## ngx-facebook

- [Documentação para Instalar](https://www.npmjs.com/package/angular2-social-login)
- [Documentação Detalhada](https://zyra.github.io/ngx-facebook/)

## Funções Executadas

- Verifica Status de Login ao abrir a página e já preenche usuário logado;
- Faz login ao clicar no botão de login;
- Faz logoff ao clicar no botão de logoff;
- Executa a API do Facebook para retornar os dados do usuário já logado;

## O que falta?

- Entender como eu valido o usuário logado via REST Api no Node.js.

Será que vou ter que passar esse access_token para minha API, e de lá validar o usuário novamente?