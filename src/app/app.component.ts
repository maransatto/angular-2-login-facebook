import { Component, OnInit } from '@angular/core';
import { FacebookService, InitParams, LoginResponse } from 'ngx-facebook';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Teste de Login via';
  status = '';
  authResponse = '';
  email = '';
  nome = '';
  id = '';
  foto = '';

  constructor(private fb: FacebookService) {
    const initParams: InitParams = {
      appId: '212213526159931',
      xfbml: true,
      version: 'v3.0'
    };

    fb.init(initParams);
  }

  ngOnInit() {
    this.fb.getLoginStatus()
      .then(response => {
        if (response.status === 'connected') {
          this.status = response.status;
          this.authResponse = JSON.stringify(response.authResponse);
          this.recuperarDadosDoFacebook();
        } else {
          this.status = 'disconnected';
        }
      })
      .catch((error: any) => console.error(error));
  }

  login(): void {
    this.fb.login({
      scope: 'public_profile,email',
      return_scopes: true
    })
      .then((response: LoginResponse) => {
        this.status = response.status;
        this.authResponse = JSON.stringify(response.authResponse);

        this.recuperarDadosDoFacebook();
      })
      .catch((error: any) => console.error(error));
  }

  logout(): void {
    this.fb.logout().then(() => {
      this.status = 'disconnected';
      this.authResponse = '';
      this.email = '';
      this.nome = '';
      this.id = '';
      this.foto = '';
    });
  }

  recuperarDadosDoFacebook() {
    this.fb.api('/me', 'get', {
      fields: 'id,about,age_range,picture,birthday,context,email,first_name,gender,link,location,middle_name,name,website,work'
    })
      .then(res => {
        this.email = res.email;
        this.nome = res.name;
        this.id = res.id;
        this.foto = res.picture.data.url;

        console.log(res);
      })
      .catch(e => console.log(e));
  }
}
